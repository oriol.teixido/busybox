#!/bin/bash

if [ "$#" -ne "1" ]; then
    echo "Arguments incorrectes."
    echo "   $0 <NAME>"
    exit 1
fi

NAME="$1"

mkdir -p /ssl
openssl genrsa -des3 -out "/ssl/${NAME}.key" 2048
openssl req -x509 -new -nodes -key "/ssl/${NAME}.key" -sha256 -days 1825 -addext basicConstraints=CA:true -out "/ssl/${NAME}.pem"