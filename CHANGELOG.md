2.6.0 / 2022-11-01
==================

  * Afegit mosquitto clients

2.5.0 / 2022-10-05
==================

  * Afegit rclone

2.4.1 / 2022-09-23
==================

  * Modificat script mysql_restore
  * Afegit script mysql_delete

2.4.0 / 2022-01-17
==================

  * Afegit telegram.py que permet enviar missatges a grups de telegram.

2.3.0 / 2022-01-17
==================

  * Imatge actualitzada

2.2.0 / 2021-10-08
==================

  * Fix. Restore mysql
  * Afegir client ssh
  * Afegir script /usr/local/bin/pg_delete

2.1.0 / 2021-08-02
==================

  * Afegir client ssh

2.0.0 / 2021-08-02
==================

  * Afegir borgbackup
  * Afegir script /usr/local/bin/pg_delete

1.0.1 / 2021-03-03
==================

  * TYPO al nom de pg_create

1.0.0 / 2021-02-25
==================

  * Versió inicial
