FROM alpine:3.18

RUN apk update \
    && apk add bash curl openssl \
    && apk add mariadb-client postgresql-client \
    && apk add borgbackup openssh-client \
    && apk add rclone \
    && apk add py3-pip py3-pip && pip3 install requests pipenv \
    && apk add mosquitto-clients 

COPY docker/ /

ENTRYPOINT ["/usr/local/bin/entrypoint"]
