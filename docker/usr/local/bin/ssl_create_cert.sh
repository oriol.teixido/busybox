#!/bin/bash

if [ "$#" -ne "1" ]; then
    echo "Arguments incorrectes."
    echo "   $0 <NAME>"
    exit 1
fi

NAME="$1.oteixido.net"

openssl genrsa -out "/ssl/${NAME}.key" 2048
openssl req -new -key "/ssl/${NAME}.key" -out "/ssl/${NAME}.csr" -subj "/C=ES/ST=Barcelona/L=Sant Adria de Besos/O=oteixido.net/CN=$NAME" -addext subjectAltName=DNS:$NAME 
openssl x509 -req -in "/ssl/${NAME}.csr" -CA "/ssl/ca.pem" -CAkey "/ssl/ca.key" -CAcreateserial -out "/ssl/${NAME}.crt" -days 365 -sha256 -extfile <(printf "subjectAltName=DNS:$NAME")
cat "/ssl/${NAME}.crt" "/ssl/ca.pem" > "/ssl/${NAME}.pem"
