#!/usr/bin/env python3
import requests
import argparse
import urllib.parse
import sys

class TelegramChat():
    token = None
    chat = None
    
    def __init__(self, token, chat):
        self.token = token
        self.chat = chat

    def send(self, message):
        url = "https://api.telegram.org/bot{token}/sendMessage".format(token=urllib.parse.quote(self.token))
        payload = {
            'chat_id': self.chat,
            'text': message,
            'parse_mode': 'HTML'
        }
        return requests.post(url, data=payload)

parser = argparse.ArgumentParser()
parser.add_argument('--token', required=True)
parser.add_argument('--chat', required=True)
args = parser.parse_args()

TelegramChat(token=args.token, chat=args.chat).send(sys.stdin.read())